The controller board assembly (EASM file) can be viewed, manipulated, measured etc in the [eDrawings](https://www.solidworks.com/product/solidworks-edrawings)** viewer. This is usually faster than importing large STEP files into 3D design tools.

_eDrawings used to be free but there are now paid and trial versions._

## 

![eDrawings Viewer](eDrawings_view.png)

## 

** ©2002-2021 Dassault Systèmes SolidWorks Corporation