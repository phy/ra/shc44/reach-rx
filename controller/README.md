## Microcontroller (uC)

<br>

Although known as the 'microcrontroller' this section also provides power supplies for everything except thermal management. Strong filtering is included. For noise critical supplies, a combination of SMPS and linear regulators are employed for optimum combination of efficiency and low noise.

With all supplies on and fully loaded, temperature rise inside the uC enclosure is only 2C.

Power from the Services Node is typically 32W fully loaded, via a 48V line (30-53V). This does not include the thermal management load.

The 'uC' comprises the controller and breakout board:

![Controller panel](uC_cover.jpg)
**Top panel**: *the cutouts and annotations were laser marked.*

<br>

![Breakout board](reach_breakout.jpg)
**Breakout board**: *connectors, additional EMI filtering, noise source regulator (28V).*

<br>

![Controller](controller_main_board.jpg)
**Controller board**: *microcontroller, power supplies and RFI filtering (yellow wire is mod for measuring external supply voltage).*


### Interfaces

* Serial over USB, 12 Mbit/sec (baud rate ignored)
* I2C (SCL, SDA) for temperature sensors and external fan control
* RS232 (Tx, Rx) for TEC controller
* Second I2C or GPIO (GP3/SCL2, GP4/SDA2) spare

### Power supplies

|Supply|Switchable|Description|
|-------|----------|-----------|
|3.3V|N|Teensy 3.5 microcontroller|
|5V|N|Fibre to USB converter (ext)|
|6V (A)|Y| Low noise amplifier (LNA)|
|6V (B)|Y| Low noise amplifier (AMP1)|
|12V|Y|VNA (ext) and RF switches|
|24V|Y|Fan (baseplate) and RF switches)|
|28V|Y|Noise source power |

The previous ability to switch the 5V supply has been disabled in hardware.

### Pinout

<img src="controller_pinout.png" width="480">


| Pin |  Description                    |
|-----|---------------------------------|
|**MSn-p**|Mechanical RF switch *n* port *p*|
| **MTS** |Mechanical RF transfer switch (aka MSX on schematics) |
| **TEC** |Thermo-Electric Controller |

### [Design files](./kicad)

KiCad files, including schematics.

<br>

![uC Assembly](uc_assembly.png)

**Board assembly**

Note there is an aluminium heat transfer bracket (not shown) between the DC-DC converter modules and the enclosure wall. Internal temperature rise, all loads on, is 2C typ.

### Top panel

Vector graphic [svg file](Reach-Breakout-Panel-v2.svg) for the second FE top panel, ready for import into *Trotec Ruby* for laser marking, where black represents engrave and cyan for annotation.
