### KiCad 7 files

Manual modifications not incorporated on the v1.x controller board:

* Disconnect 5V_EN at U1 pin 48 (D21) so that 5V supply is always on to maintain comms via the USB fibre converter.
* Vin to ADC (A22) divider network (180k + 10k resistor) to sense voltage at the enclosure DC supply in.
* Battery (CR2032) for the Teensy RTC
* Temp sensor (MCP9808) board.

**Note:** references to:
Mechanical Switch Crossover (MSX) and
Mechanical Transfer Switch (MTS)
are used interchangeably
