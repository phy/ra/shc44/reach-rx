#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

#include "reset.h"
#include "cmd.h"

char reset[] = "reset";  // avoids warning: ISO C++ forbids converting string constant to char*
CMD cmd_reset = {
    name : reset,
    init : &init_reset,
    exec : &exec_reset,
    help : &help_reset
};

// List of RESET sub command names
const char *reset_args[] = {
  "now"
};

int init_reset() {
  return 0;
}

int help_reset() {
  Serial.println("\nThis performs a software reset, restoring default settings.");
  Serial.println("     reset now\n");
  return 0;
}

int exec_reset() {
  if (strcmp(args[1], reset_args[0]) == 0) {
    Serial.println("Performing software reset");
    delay(1000);
    // System Control Block, Application Interrupt and Reset Control Register
    // valid for all ARM Cortex-M processors
    SCB_AIRCR = 0x05FA0004;
    return 0;  // never gets here
  }
  else {
    Serial.println("Invalid command");
    return 1;
  }
}
