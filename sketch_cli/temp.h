#ifndef TEMP_H
#define TEMP_H

#include "cmd.h"

extern CMD cmd_temp;
extern bool hasTempSensor0;
extern float getTempSensor0();

int init_temp();
int help_temp();
int exec_temp();

#endif
