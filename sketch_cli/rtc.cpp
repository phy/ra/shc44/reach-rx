#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <TimeLib.h>
#include <EEPROM.h>

#include "cmd.h"
#include "rtc.h"

char time[] = "time";  // avoids warning: ISO C++ forbids converting string constant to char*
CMD cmd_time = {
    name : time,
    init : &init_time,
    exec : &exec_time,
    help : &help_time
};

time_t alarmTime;      // store last over temperature event
int eeAddress = 0x10;  // change on re-compile for wear levelling, 4 byte increments
time_t startTime;
unsigned long elapsedTime;

// List of TIME sub command names
const char *time_args[] = {
  "adj",
  "alarm",
  "elapsed",
  "set",
  "sync",
  "h",
  "diff"
};

time_t getTeensy3RTC()  {
  return Teensy3Clock.get();
}

void printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void timeDisplay() {
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(dayShortStr(weekday()));
  Serial.print(" ");
  Serial.print(day());
  Serial.print(" ");
  Serial.print(monthShortStr(month()));
  Serial.print(" ");
  Serial.print(year());
  Serial.println(); 
}

void alarmDisplay() {
  EEPROM.get(eeAddress, alarmTime);
  Serial.print(hour(alarmTime));
  printDigits(minute(alarmTime));
  printDigits(second(alarmTime));
  Serial.print(" ");
  Serial.print(dayShortStr(weekday(alarmTime)));
  Serial.print(" ");
  Serial.print(day(alarmTime));
  Serial.print(" ");
  Serial.print(monthShortStr(month(alarmTime)));
  Serial.print(" ");
  Serial.print(year(alarmTime));
  Serial.println(); 
}

void elapsedDisplay() {
  int n = elapsedTime;
  Serial.print(int(n / (24 * 3600)));
  Serial.print(" days ");
  n = n % (24 * 3600);
  Serial.print(int(n / 3600));
  Serial.print(" hrs ");
  n %= 3600;
  Serial.print(int(n / 60));
  Serial.print(" mins ");
  n %= 60;
  Serial.print(n);
  Serial.println(" s");
}


int init_time() {
  // set system time from RTC
  setSyncProvider(getTeensy3RTC);
  setSyncInterval(31536000);  // 1 year (basically never), max 136 years
  
  if (timeStatus()!= timeSet) {
    Serial.println("Unable to sync with the RTC");
  } else {
    startTime = now();
    Serial.print("RTC: ");
    timeDisplay();
  }
  return 0;
}


int help_time() {
  // Note it is intended that allowing the system clock and RTC to drift, checking
  // the clock difference can be used as a system health check.
  Serial.println("\nCommands for checking and adjusting system and RTC clocks:");
  Serial.println("System clock is a microcontroller firmware clock based on millis().");
  Serial.println("RTC is an always running, battery backed-up clock using a 32kHz crystal.");
  Serial.println("1) Get system time:");
  Serial.println("     time      (Unix time)");
  Serial.println("     time h    (human readable)");
  Serial.println("2) Get time since last start/reboot:");
  Serial.println("     time elapsed     (seconds)");
  Serial.println("     time elapsed h   (human readable)");
  Serial.println("3) Get last over-temperature event time:");
  Serial.println("     time alarm       (Unix time)");
  Serial.println("     time alarm h     (human readable)");
  Serial.println("     time alarm reset  (read returns -1 (Unix) or \"never\" (human))");
  Serial.println("4) Get time difference in seconds (system - RTC):");
  Serial.println("     time diff");
  Serial.println("5) Adjust system time with an offset in seconds:");
  Serial.println("     time adj <offset>");
  Serial.println("     time adj -3600 (set system time back 1 hour)");
  Serial.println("6) Set RTC from the microcontroller system time:");
  Serial.println("     time set    (system time --> RTC)");
  Serial.println("7) Sync microcontroller system time with the RTC:");
  Serial.println("     time sync   (system time <-- RTC)\n");
  return 0;
}

int exec_time() {
  // Adjust
  if (strcmp(args[1], time_args[0]) == 0) {
    if (abs(atoi(args[2])) < now()) {
      adjustTime(atoi(args[2]));
      Serial.print("Adjusted ");
      Serial.print(args[2]);
      Serial.println(" seconds");
    }
    else {
      Serial.println("Invalid adjustment");
    }
  }
  // get last alarm time
  else if (strcmp(args[1], time_args[1]) == 0) {
    alarmTime = EEPROM.get(eeAddress, alarmTime);
    if (alarmTime < 0) {
      alarmTime = -1;   // 0xff (blank EEPROM) returns -1
    }
    if (strcmp(args[2], "") == 0) {
      Serial.println(alarmTime);
    }
    else if (strcmp(args[2], "h") == 0) {
      if (alarmTime >= 0) {
        alarmDisplay();
      }
      else {
        Serial.println("never");
      }
    }
    else if (strcmp(args[2], "reset") == 0) {
      for (int i = eeAddress; i < eeAddress + 4; i++) {
        EEPROM.write(i, 0xFF);
      }
    Serial.println("Reset");
    }
  }
  // get elapsed time
  else if (strcmp(args[1], time_args[2]) == 0) {
    elapsedTime = now() - startTime;
    if (strcmp(args[2], "") == 0) {
      Serial.println(elapsedTime);
    }
    else if (strcmp(args[2], "h") == 0) {
      elapsedDisplay();
    }
  }
  // Set
  else if (strcmp(args[1], time_args[3]) == 0) {
    Teensy3Clock.set(now());
    if (timeStatus() == timeSet) {
      Serial.println("Set");
    }
    else {
      Serial.println("Set failed");
    }
  }
  // Synchronise
  else if (strcmp(args[1], time_args[4]) == 0) {
    setSyncProvider(getTeensy3RTC);
    setSyncInterval(31536000);   // 1 year (ie never)
    if (timeStatus() == timeSet) {
      Serial.println("Synchronised");
    }
    else {
      Serial.println("Sync failed");
    }
  }
  // Get human readable time
  else if (strcmp(args[1], time_args[5]) == 0) {
    timeDisplay();
  }
  // Get Unix time
  else if (strcmp(args[1], "") == 0) {
    Serial.println(now());
  }
  // Get system - Unix difference in seconds
  else if (strcmp(args[1], time_args[6]) == 0) {
    int diff = now() - Teensy3Clock.get();
    Serial.println(diff);
  }
  else {
    Serial.println("Invalid command. Type \"help time\" to see how to use the TIME command.");
  }
  return 0;
}

time_t getAlarmTime() {
  return EEPROM.get(eeAddress, alarmTime);
}

int storeAlarmTime() {
  EEPROM.put(eeAddress, now());
  return 0;
}
