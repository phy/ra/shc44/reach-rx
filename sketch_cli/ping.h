#ifndef PING_H
#define PING_H

#include "cmd.h"

extern CMD cmd_ping;

int init_ping();
int help_ping();
int exec_ping();

#endif
