#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <ADC.h>
#include <ADC_util.h>

#include "supply.h"
#include "cmd.h"

const int readPin = A22; // ADC1
ADC *adc = new ADC();    // adc object


char supply[] = "supply";  // avoids warning: ISO C++ forbids converting string constant to char*
CMD cmd_supply = {
  name : supply,
  init : &init_supply,
  exec : &exec_supply,
  help : &help_supply
};

int init_supply() {
  return 0;
}


int help_supply() {
  Serial.println("\nGet front-end DC supply voltage.\n");
  return 0;
}


int exec_supply() {
  int value = adc->adc1->analogRead(readPin);
  Serial.println(value*62.7/adc->adc1->getMaxValue(), 2);  // 3.3V ref, 19:1 divider --> 62.7

  return 0;
}
