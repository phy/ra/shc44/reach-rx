#ifndef RTC_H
#define RTC_H

#include "cmd.h"

extern CMD cmd_time;
extern int storeAlarmTime();
extern time_t getAlarmTime();

int init_time();
int help_time();
int exec_time();

#endif
