#ifndef SUPPLY_H
#define SUPPLY_H

#include "cmd.h"

extern CMD cmd_supply;

int init_supply();
int help_supply();
int exec_supply();

#endif
