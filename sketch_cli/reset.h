#ifndef RESET_H
#define RESET_H

#include "cmd.h"

extern CMD cmd_reset;

int init_reset();
int help_reset();
int exec_reset();

#endif
