#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

#include "cmd.h"
#include "temp.h"
#include "Adafruit_MCP9808.h"

char temp[] = "temp";  // avoids warning: ISO C++ forbids converting string constant to char*
CMD cmd_temp = {
  name : temp,
  init : &init_temp,
  exec : &exec_temp,
  help : &help_temp
  };
  
bool hasTempSensor0 = false;

Adafruit_MCP9808 sensor0 = Adafruit_MCP9808();
Adafruit_MCP9808 sensor1 = Adafruit_MCP9808();
Adafruit_MCP9808 sensor2 = Adafruit_MCP9808();
Adafruit_MCP9808 sensor3 = Adafruit_MCP9808();
Adafruit_MCP9808 sensor4 = Adafruit_MCP9808();
Adafruit_MCP9808 sensor5 = Adafruit_MCP9808();
Adafruit_MCP9808 sensor6 = Adafruit_MCP9808();
Adafruit_MCP9808 sensor7 = Adafruit_MCP9808();


int init_temp() {
  int sensorCount = 0;
  Serial.println("Temp sensor (i2c) init:");

  if (sensor0.begin(0x18)) {
    hasTempSensor0 = true;
    sensorCount += 1;
    Serial.println("   0 (addr 0x18) found");
  } 
  if (sensor1.begin(0x19)) {
    sensor1.shutdown();
    sensorCount += 1;
    Serial.println("   1 (addr 0x19) found");
  }
  if (sensor2.begin(0x1A)) {
    sensor2.shutdown();
    sensorCount += 1;
    Serial.println("   2 (addr 0x1A) found");
  } 
  if (sensor3.begin(0x1B)) {
    sensor3.shutdown();
    sensorCount += 1;
    Serial.println("   3 (addr 0x1B) found");
  } 
  if (sensor4.begin(0x1C)) {
    sensor4.shutdown();
    sensorCount += 1;
    Serial.println("   4 (addr 0x1C) found");
  } 
  if (sensor5.begin(0x1D)) {
    sensor5.shutdown();
    sensorCount += 1;
    Serial.println("   5 (addr 0x1D) found");
  } 
  if (sensor6.begin(0x1E)) {
    sensor6.shutdown();
    sensorCount += 1;
    Serial.println("   6 (addr 0x1E) found");
  } 
  if (sensor7.begin(0x1F)) {
    sensor7.shutdown();
    sensorCount += 1;
    Serial.println("   7 (addr 0x1F) found");
    }

  if (sensorCount > 0) {
    Serial.print(sensorCount);
    Serial.println(" sensors found");
  }
  else {
    Serial.println("none found");
  }
  return 0;
}

int help_temp() {
    Serial.println("\nTemperature sensor (MCP9808), examples:");
    Serial.println("1) Read temperature in celsius:");
    Serial.println("     temp <id>");
    Serial.println("        where \"<id>\" is the i2c address index:");
    Serial.println("        0   0x18");
    Serial.println("        1   0x19");
    Serial.println("        2   0x1A");
    Serial.println("        3   0x1B");
    Serial.println("        4   0x1C");
    Serial.println("        5   0x1D");
    Serial.println("        6   0x1E");
    Serial.println("        7   0x1F");
    Serial.println("2) Re-initialise and list available sensors:");
    Serial.println("     temp init\n");
    return 0;
}

int exec_temp() {
    if (strcmp(args[1], "0") == 0) {
        // Keep sensor 0 (uC enclosure) from sleeping
        Serial.println(sensor0.readTempC());
        return 0;
    } else if (strcmp(args[1], "1") == 0) {
        sensor1.wake();
        Serial.println(sensor1.readTempC());
        sensor1.shutdown();
        return 0;
    } else if (strcmp(args[1], "2") == 0) {
        sensor2.wake();
        Serial.println(sensor2.readTempC());
        sensor2.shutdown();
        return 0;
    } else if (strcmp(args[1], "3") == 0) {
        sensor3.wake();
        Serial.println(sensor3.readTempC());
        sensor3.shutdown();
        return 0;
    } else if (strcmp(args[1], "4") == 0) {
        sensor4.wake();
        Serial.println(sensor4.readTempC());
        sensor4.shutdown();
        return 0;
    } else if (strcmp(args[1], "5") == 0) {
        sensor5.wake();
        Serial.println(sensor5.readTempC());
        sensor5.shutdown();
        return 0;
    } else if (strcmp(args[1], "6") == 0) {
        sensor6.wake();
        Serial.println(sensor6.readTempC());
        sensor6.shutdown();
        return 0;
    } else if (strcmp(args[1], "7") == 0) {
        sensor7.wake();
        Serial.println(sensor7.readTempC());
        sensor7.shutdown();
        return 0;
    } else if (strcmp(args[1], "init") == 0) {
        init_temp();
        return 0;
    } else {
        Serial.print("Invalid temp command: ");
        Serial.println(args[1]);
        return 1;
    }
}

float getTempSensor0() {
  return(sensor0.readTempC());
}
