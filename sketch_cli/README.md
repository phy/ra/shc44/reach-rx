## Firmware

Arduino sketch for the Teensy 3.5 (ARM Cortex-M4) microcontroller firmware. This model was chosen as the most powerful (in 2019) while having 5V tolerant GPIO pins.

**Cloned (25 May 2021) from Tian Huang's excellent work at Cambridge.**

https://github.com/ianmalcolm/sketch_cli

### CLI Commands

![CLI Commands](firmware_commands.png)

**Over-temperature protection:** Microcontroller enclosure temperature is measured once a minute. If 
three consecutive measurements are above the preset alarm temperature (58C) the following 
events are triggered:

- Pin 20 (internal fan) and pin 23 (24V supply) are enabled for cooling 
- all other power supplies are disabled 
- the event date and time is stored in EEPROM.

**Time:** events are timed with a firmware clock function using Arduino millis(). This has fast access but 
is prone to drifting. At power-on or reset, the firmware clock is set from a battery backed up Real 
Time Clock (RTC). RTC access is much slower, hence two clocks and the provision to read the time 
difference, or to set one clock from the other. The RTC can be adjusted by adding an offset to the 
firmware clock, followed by the command ‘time set’.

### GPIO Pinout

<img src="../controller/controller_pinout.png" width="480">
