#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

#include "cmd.h"
#include "gpio.h"
#include "help.h"
#include "supply.h"
#include "reset.h"
#include "rtc.h"
#include "temp.h"
#include "ping.h"

#define LINE_BUF_SIZE 128    //Maximum input string length
#define ARG_BUF_SIZE 64      //Maximum argument string length
#define MAX_NUM_ARGS 8       //Maximum number of arguments

char line[LINE_BUF_SIZE];
char args[MAX_NUM_ARGS][ARG_BUF_SIZE];

float alarmTemp = 58;        //Temperature (C) to trigger shutdown
const int maxAlarmCount = 3;
int alarmCount = 0;

CMD cmds[] = {
  cmd_gpio,
  cmd_help,
  cmd_ping,
  cmd_reset,
  cmd_supply,
  cmd_time,
  cmd_temp
};

bool error_flag = false;
bool timer_flag = false;
int ncmd = sizeof(cmds) / sizeof(CMD);

CMD* find_cmd(char *s){
  CMD *p = NULL;
  for (int i=0; i<ncmd; i++) {
    if (strcmp(s,cmds[i].name) == 0) {
      p = &cmds[i];
      break;
    } else {
      continue;
    }
  }
  return p;
}

IntervalTimer timer1;

void setup() {
  Serial.begin(115200);
  for (int i=0; i<ncmd; i++) {
    cmds[i].init();
  }
  timer1.begin(setTimerFlag, 60000000);  // every 60s
  cli_init();
}


void loop() {
    my_cli();
}


void cli_init() {
  Serial.println("\nWelcome to this simple Arduino command line interface (CLI)");
  Serial.println("running on a Teensy 3.5 microcontroller (ARM Cortex-M4F).\n");
  Serial.print("Note if the microcontroller enclosure temperature rises above ");
  Serial.print(alarmTemp, 0);
  Serial.print("C for more than ");
  Serial.print(maxAlarmCount);
  Serial.println(" minutes");
  Serial.println("all outputs will switch off except the internal fan and 24V supply to allow the system to");
  Serial.println("cool. This is to protect the electronics in the event of operation without thermal control.");
  Serial.println("Use the command \"temp 0\" to check the current microcontroller enclosure temperature.\n");
  Serial.println("Type \"help\" to see a list of commands.\n");
}

void my_cli() {
  //Serial.print("> ");
  read_line();
  if (!error_flag) {
    parse_line();
  }
  if (!error_flag) {
    execute();
  }
  memset(line, 0, LINE_BUF_SIZE);
  memset(args, 0, sizeof(args[0][0]) * MAX_NUM_ARGS * ARG_BUF_SIZE);
  error_flag = false;
}

void read_line() {
  String line_string;
  // The uC spends most of its time in this while loop waiting for input. If the uC is
  // waiting for input AND the 1min timer flag is set, check the uC for over temperature:
  while (!Serial.available()) {
    if (timer_flag) {                    // 1 minute interupt timer
      timer_flag = false;
      if (hasTempSensor0) {
        float temp0 = getTempSensor0();  // in temp.cpp (takes 250us)
        if (temp0 < alarmTemp) {
          alarmCount = 0;
        }
        else {                           // print over temp warning
          alarmCount += 1;
          if (alarmCount < maxAlarmCount) {
            Serial.print("Overtemp warning ");
            Serial.print(alarmCount);
            Serial.print(": ");
            Serial.println(temp0);
          }
          else {  // switch off supplies (except fan) and store time in EEPROM
            Serial.print("Overtemp shutdown: ");
            Serial.println(temp0);
            gpioShutdown();        // in gpio.cpp
            storeAlarmTime();      // in rtc.cpp
          }
        }
      }
    }
  }
  // Input is available, parse command line
  if (Serial.available()) {
    line_string = Serial.readStringUntil('\n');
    if (line_string.length() < LINE_BUF_SIZE) {
      line_string.toCharArray(line, LINE_BUF_SIZE);
      //Serial.println(line_string);
    }
    else {
      Serial.println("Input string too long.");
      error_flag = true;
    }
  }
}

void parse_line() {
  char *argument;
  int counter = 0;

  argument = strtok(line, " ");

  while ((argument != NULL)) {
    if (counter < MAX_NUM_ARGS) {
      if (strlen(argument) < ARG_BUF_SIZE) {
        strcpy(args[counter], argument);
        argument = strtok(NULL, " ");
        counter++;
      }
      else {
        Serial.println("Input string too long.");
        error_flag = true;
        break;
      }
    }
    else {
      break;
    }
  }
}

int execute() {
  CMD *pcmd = find_cmd(args[0]);
  if (pcmd!=NULL) {
    return pcmd->exec();
  } else {
    Serial.println("Invalid command. Type \"help\" for more.");
    return 0;
  }
}

void setTimerFlag() {
  timer_flag = true;
}
