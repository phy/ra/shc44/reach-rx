#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

#include "ping.h"
#include "cmd.h"

char ping[] = "ping";  // avoids warning: ISO C++ forbids converting string constant to char*
CMD cmd_ping = {
    name : ping,
    init : &init_ping,
    exec : &exec_ping,
    help : &help_ping
};


int init_ping() {
  return 0;
}


int help_ping() {
  Serial.println("\nReturns \"OK\" which can be used as a system health check.\n");
  return 0;
}


int exec_ping() {
  Serial.println("OK");
  return 0;
}
