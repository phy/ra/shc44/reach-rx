#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

#include "cmd.h"
#include "gpio.h"

char gpio[] = "gpio";  // avoids warning: ISO C++ forbids converting string constant to char*
CMD cmd_gpio = {
    name : gpio,
    init : &init_gpio,
    exec : &exec_gpio,
    help : &help_gpio
};

// List of GPIO pins to be set to output mode:
const int GPIO_PIN[] = {
  2,  3,  4,  5,  6,  9,  10, 11,
  12, 14, 15, 16, 17, 20, 22, 23,
  24, 25, 26, 27, 28, 29, 30, 31,
  32, 33, 34, 35, 36, 37, 38
};

// List of GPIO pins to set off (low) in case of thermal shutdown:
const int GPIO_OFF[] = {
  3,  4,  5,  6,  9,  10, 11,
  12, 14, 15, 16, 17, 22, 24,
  25, 26, 27, 28, 29, 30, 31,
  32, 33, 34, 35, 36, 37, 38
};

int init_gpio() {
    // Initialize gpio
    for (int i = 0; i < int(sizeof(GPIO_PIN)) / int(sizeof(GPIO_PIN[0])); i++) {
        pinMode(GPIO_PIN[i], OUTPUT);
    }
    return 0;
}

int gpioShutdown() {
    // Turn off (low) gpio pins to reduce power, except 24V and internal fan:
    for (int i = 0; i < int(sizeof(GPIO_OFF)) / int(sizeof(GPIO_OFF[0])); i++) {
        digitalWriteFast(GPIO_OFF[i], LOW);
    }
    // For cooling: ensure 24V and internal fan is on:
    digitalWriteFast(23, HIGH);  // 24V
    digitalWriteFast(20, HIGH);  // INT_FAN
    return 0;
}

int help_gpio() {
    Serial.println("\nRead or write the GPIO. Valid GPIO numbers range from 2 to 38 (except 21).");
    Serial.println("Note writing to a GPIO pin allocated to a mechanical switch will automatically");
    Serial.println("set all other pins for that switch to OFF.");
    Serial.println("To read GPIO pin 22:");
    Serial.println("   gpio 22");
    Serial.println("To write 1 to GPIO pin 38:");
    Serial.println("   gpio 38 1\n");

    return 0;
}

int exec_gpio() {

    /* Convert the provided value to a decimal long */
    long int gpio_pin = atol(args[1]);

    if (gpio_pin == 0) {
        // nothing parsed from the string, handle errors or exit
        Serial.println("Invalid pin 0 or conversion error occurred.");
        return 1;
    }

    if (gpio_pin>38 || gpio_pin<2  || gpio_pin==21) {
        // out of range, handle or exit (gpio 21 was 5V enable)
        Serial.println("The value provided was not allowed.");
        return 1;
    }

    if (strcmp(args[2], "0") == 0) {
        digitalWriteFast(gpio_pin, 0);
    } else if (strcmp(args[2], "1") == 0) {
        if (gpio_pin >= 24 && gpio_pin <= 31) {    // Are we writing in the MS1 port range?
          for (int j = 24; j <= 31; j++) {         // If so, write 0 to all MS1 pins
            digitalWriteFast(j, 0);                    // since only one pin can be on at a time
          }
        }
        if (gpio_pin >= 33 && gpio_pin <= 38) {    // Are we writing in the MS2 port range?
          for (int j = 33; j <= 38; j++) {         // If so, write 0 to all MS2 pins
            digitalWriteFast(j, 0);                    // since only one pin can be on at a time
          }
        }
        if (gpio_pin >= 14 && gpio_pin <= 17) {    // Are we writing in the MS3 port range?
          for (int j = 14; j <= 17; j++) {         // If so, write 0 to all MS3 pins
            digitalWriteFast(j, 0);                    // since only one pin can be on at a time
          }
        }
        if (gpio_pin >= 9 && gpio_pin <= 12) {     // Are we writing in the MS4 port range?
          for (int j = 9; j <= 12; j++) {          // If so, write 0 to all MS4 pins
            digitalWriteFast(j, 0);                    // since only one pin can be on at a time
          }
        }
        digitalWriteFast(gpio_pin, 1);
    } else if (strcmp(args[2], "") == 0) {
        int val = digitalReadFast(gpio_pin);
        Serial.println(val);
    } else {
        Serial.println("Invalid command.");
    }

    return 0;
}
