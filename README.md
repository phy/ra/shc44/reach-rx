# REACH front end

Project details:  
https://www.astro.phy.cam.ac.uk/research/research-projects/reach

<br>

## Hardware

#### [Enclosure](./enclosure)
- 3D model of the enclosure and thermal assembly. Individual parts can be measure and extracted as STL files.

- Baseplate removal and assembly [instructions](enclosure/baseplate_removal.pdf) (required for shipping).

#### [Microcontroller (uC)](./controller)  

- Power supplies, EMC filtering, low level monitoring, over-temperature safety shutdown.

- Low-level communication (Teensy 3.5 microcontroller).

- GPIO pin assignment [table](controller/controller_pinout.png).

#### [Thermal management](./thermal)

- Custom power supply and fan control for the TEC controller.

#### [RF Switches](./rf_switches)

- Driver boards for the electro-mechanical RF switches. By placing the drivers as close as possible to the switches, current loop area for the back EMF dump current is reduced and RFI is minimised.

<br>

## Firmware

#### [Sketch CLI](./sketch_cli)  

- Firmware for the Teensy 3.5 (ARM Cortex-M4) microcontroller  .

- Table of low-level communication [commands](sketch_cli/firmware_commands.pdf).

<br>

---

<br>

![Controller](diagram.png)

*System diagram - Karoo site*
