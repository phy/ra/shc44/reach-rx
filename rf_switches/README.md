## RF Switches

<br>

Mechanical RF switches and drivers for the REACH spectrometer. Solenoid switching creates RFI from the resonant circuit comprised of; solenoid inductuctance, back EMF diode and parasitic board capacitance. By keeping these paths as short as possible (placing the drive circuit on the mechanical switch), EMI is reduced.

![4way switch](RF_SW_assy_4way.png)

### Versions

| Ways | Mini-Circuits p/n | Mounting adapter | Voltage |
|------|--------------------|------------------|---------|
| 4 | MSP4TA-18-12D+ | RF_switch_mount_small | 12V |
| 6 | MSP6TA-12D+ | RF_switch_mount_medium | 24V |
| 8 | MSP8TA-12-12D+ | RF_switch_mount_large | 12V |

### Driver voltage selection
This arrangement prevents damage in the event a 12V switch is plugged (controller end) into a 24V output since 12 and 24V supplies are carried on different wires.

![4 way switch](rf_switch_driver_back.png)

*Voltage selection.*

<br>

![8 way assembly](rf_switch_8_way_assy.jpg)

*8 way RF switch assembly (no shield foil).*

Not shown is the mylar film and copper tape shield.
