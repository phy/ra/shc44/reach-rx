## Thermal management

<br>

Power supply for the thermo-electric cooler. Input voltage range is 32 to 53V (lower limit set by the electronic noise source). The load is typically 88W at 22V.

The core SMPS module is capable of 300W however other parts limit this to 200W and a reasonable derating for reliability informs a maximum cooling power of approximately 150W.

The PSU also switches on the external fans when the TEC controller draws more than approximately 6W.

Note it is appropriate to measure demand in terms of power rather than current since the PSU accepts a wide voltage range with high power conversion efficiency (95% typ). Input current has a correspondingly large range depending on both input voltage and demand. The constant is power.

The TEC temperature controller is an Electron Dynamics Ltd (Southampton) model TC-M-U-10A.

The peltier module is a Laird UltraTEC model UT6-24-F1-5555, rated 113W max. However this is operating just past the point of maximum efficiency. As suggested in 2022 [1] it would benefit from a higher power version (UTX6-24-F1-5555-TA-W6).

<br>

![TEC PSU board](tec_psu_board.png)

*Custom PSU board for the thermal controller.*

![](TEC_PSU_1.2.jpg)

**Note:** R12, R13 have been replaced by a 0.75A resettable fuse to protect against a fault with the external fan (RS part number: 1740762).

Enclosure holes are now covered with copper tape to reduce RFI, at the expense of a slight increase in operating temperature.

### Design files

KiCad [files](./kicad) including schematic.

<br>

---

[1] Carey, S. (2022). *REACH receiver front-end control and monitoring.* Observational and Theoretical 21-cm Cosmology Conference. University of Cambridge.

‌