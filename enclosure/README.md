## Enclosure

<br>

![Enclosure](THERMAL_ASSEMBLY_V2_X.PNG)

Insulated stainless steel enclosure with internal airflow around the secondary baseplate supporting receiver electronics.

- Rittal 1007.600 enclosure (500 x 500 x 210mm)
- Kingspan Kooltherm K5 20mm wall board insulation (19mm nominal)
- top brackets for M8 threaded rod connection to ground plane
- bottom brackets (optional) for stability.

The 3D model was created with SolidWorks 2016 [(eDrawing version here)](THERMAL_ASSEMBLY_V2.EASM). It can be imported into Inventor, or opened with the [free eDrawings viewer](http://www.edrawingsviewer.com/). This model has full permissions to measure and extract parts (STL files).